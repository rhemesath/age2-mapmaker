import tkinter as tk
import AGameEngine2 as ag

class Command:
    def do(self): pass
    def undo(self): pass

class CommandManager:
    def __init__(self, main):
        self.main = main
        self.cmds = []
        self.i = -1 # always at last executed cmd. Is -1 if max undone
        self.l = 0  # len(self.cmds)
        self.savedI = -1
        self.menu = main.menuBar
        self.__updateMenu()

        main.win.bind("<Control-z>", lambda *a: self.undo() )
        main.win.bind("<Control-y>", lambda *a: self.redo() )

    def add(self, cmd):
        if self.i != self.l - 1:
            self.cmds = self.cmds[ : self.i + 1]
        self.cmds.append(cmd)
        self.l = len(self.cmds)
        self.i = self.l - 1
        self.__updateMenu()

    def do(self, cmd):
        cmd.do()
        self.add(cmd)

    def undo(self):
        if self.i == -1: return
        self.cmds[self.i].undo()
        self.i -= 1
        self.__updateMenu()

    def redo(self):
        if self.i == self.l - 1: return
        self.i += 1
        self.cmds[self.i].do()
        self.__updateMenu()

    def __updateMenu(self):
        if self.i == -1: self.menu.entryconfigure("⟲", state="disabled")
        else: self.menu.entryconfigure("⟲", state="normal")
        if self.i == self.l - 1: self.menu.entryconfigure("⟳", state="disabled")
        else: self.menu.entryconfigure("⟳", state="normal")
        self.main.updateTitle()
    
    def isUnsaved(self) -> bool:
        return self.i != self.savedI
    
    def save(self): self.savedI = self.i

class SetTileCmd(Command):
    def __init__(self, layer, index: int, x: int, y: int):
        self.layer = layer
        self.index = index
        self.x = x
        self.y = y
        self.prevTile = None
    
    def do(self):
        self.prevTile = self.layer.getTile(self.x, self.y)
        if self.prevTile == -1: return
        self.layer.setTile(self.index, self.x, self.y)
    
    def undo(self):
        if self.prevTile == -1: return
        self.layer.setTile(self.prevTile, self.x, self.y)

class SetTilesCmd(Command):
    def __init__(self, layer, index: int):
        self.layer = layer
        self.index = index
        self.tiles = []
        self.cmds = []
        self.isUsed = False

    def add(self, x: int, y: int):
        if (x, y) in self.tiles: return
        self.isUsed = True
        self.tiles.append((x, y))
        cmd = SetTileCmd(self.layer, self.index, x, y)
        cmd.do()
        self.cmds.append(cmd)

    def addLine(self, x1: int, y1: int):
        """ implements Bresenham's line drawing algorithm """
        if len(self.cmds) == 0:
            self.add(x1, y1)
            return

        x0, y0 = self.tiles[-1]

        dx =  abs(x1 - x0)
        dy = -abs(y1 - y0)
        sx = 1 if x0 < x1 else -1
        sy = 1 if y0 < y1 else -1
        err = dx + dy
        while True:
            self.add(x0, y0)
            if x0 == x1 and y0 == y1: break
            e2 = err * 2
            if e2 >= dy:
                err += dy
                x0 += sx
            if e2 <= dx:
                err += dx
                y0 += sy

    def do(self):
        for tile in self.cmds: tile.do()

    def undo(self):
        for tile in self.cmds: tile.undo()

class SetImageFromPathCmd(Command):
    def __init__(self, main, tileSize, imgPath):
        self.main = main
        self.tileSize = tileSize
        self.imgPath = imgPath
        self.prevImgPath = main.imgPath
        self.prevTex = main.imagesTex
        self.prevTileSize = main.ts

        if imgPath is None: self.tex = ag.createTexture(self.tileSize, self.tileSize)
        else: self.tex = ag.loadTexture(imgPath)
    
    def do(self):
        self.main.ts = self.tileSize
        self.main.setImage(self.imgPath, self.tex)
    
    def undo(self):
        self.main.ts = self.prevTileSize
        self.main.setImage(self.prevImgPath, self.prevTex)

import tkinter as tk

class Mouse:
    def __init__(self, main):
        self.main = main
        self.x = 0
        self.y = 0
        self.dx = 0
        self.dy = 0

        embed = main.embed
        embed.bind("<Motion>", self.__onMotion)
        
        embed.bind("<ButtonPress-1>", self.__onLeftDown)
        embed.bind("<ButtonPress-3>", self.__onRightDown)
        embed.bind("<ButtonRelease-1>", self.__onLeftUp)
        embed.bind("<ButtonRelease-3>", self.__onRightUp)
        
        embed.bind("<MouseWheel>", self.__onScrollY)
        embed.bind("<ButtonPress-4>", self.__onScrollUp)
        embed.bind("<ButtonPress-5>", self.__onScrollDown)

        self.leftEvent  = False
        self.rightEvent = False
        self.left  = False
        self.right = False
        self.scrollY = 0

    def flush(self):
        self.leftEvent  = False
        self.rightEvent = False
        self.dx = 0
        self.dy = 0
        self.scrollY = 0

    def __onMotion(self, event):
        prevX = self.x
        prevY = self.y
        self.x = event.x
        self.y = event.y
        self.dx = self.x - prevX
        self.dy = self.y - prevY

    def __onLeftDown(self, event):
        self.leftEvent = True
        self.left = True
    def __onRightDown(self, event):
        self.rightEvent = True
        self.right = True

    def __onLeftUp(self,  event): self.left  = False
    def __onRightUp(self, event): self.right = False

    def __onScrollY(self, event):
        self.scrollY = -1 if event.delta < 0 else 1
    
    def __onScrollUp(self, event):   self.scrollY =  1
    def __onScrollDown(self, event): self.scrollY = -1

import AGameEngine2 as ag
from PIL import Image, ImageTk
from commands import *

class Cursor:
    def __init__(self, main):
        self.kb_zoom_factor = 0.2
        self.scroll_factor = 0.1
        self.previewRenderSize = 16*3 * 4
        self.miniBackgroundColor = (60, 60, 60, 255)

        self.main = main
        self.mouse = main.mouse
        self.kb = main.kb
        self.ts = main.ts
        self.cmdMan = main.cmdMan
        self.layerMan = main.layerMan

        self.x = 0.0 # 0.0 -> layer.w
        self.y = 0.0
        self.tileX = 0 # 0 -> layer.tileW
        self.tileY = 0
        self.renX = 0
        self.renY = 0
        self.layerMoveDeltaX = None
        self.layerMoveDeltaY = None

        self.index = 0
        self.indexX = 0
        self.indexY = 0

        self.previewSize = self.ts * 3
        self.previewTex = ag.createTexture(self.previewSize, self.previewSize)
        self.previewAniShow = False
        self.previewAniCounter = 0
        self.previewAniWait = 30
        self.setMiniTex(main.imagesTex)

        self.setTilesCmd = None

    def setMiniTex(self, tex):
        self.miniTex = tex
        self.miniTileW = ag.getTextureWidth(tex)  // self.ts
        self.miniTileH = ag.getTextureHeight(tex) // self.ts
        self.setIndex(0, 0)

    def update(self):
        layer = self.layerMan.get()
        
        if   self.kb.getEvent("+"): layer.addScale(1+self.kb_zoom_factor)
        elif self.kb.getEvent("-"): layer.addScale(1-self.kb_zoom_factor)
        elif self.mouse.scrollY != 0:
            layer.addScale(1 + self.mouse.scrollY * self.scroll_factor)

        if self.mouse.rightEvent:
            self.layerMoveDeltaX = self.mouse.x - layer.x
            self.layerMoveDeltaY = self.mouse.y - layer.y
        if self.mouse.right:
            layer.setX(self.mouse.x - self.layerMoveDeltaX)
            layer.setY(self.mouse.y - self.layerMoveDeltaY)
        else:
            self.__update_pos(layer)

        if   self.kb.getEvent("a"): self.setIndexLeft()
        elif self.kb.getEvent("d"): self.setIndexRight()
        elif self.kb.getEvent("w"): self.setIndexUp()
        elif self.kb.getEvent("s"): self.setIndexDown()

        if self.mouse.left: self.__placeTile(layer)
        elif self.setTilesCmd is not None and self.setTilesCmd.isUsed:
            self.cmdMan.add(self.setTilesCmd)
            self.setTilesCmd = None
        
        self.previewAniCounter += 1
        if self.previewAniCounter == self.previewAniWait:
            self.previewAniCounter = 0
            self.previewAniShow = not self.previewAniShow
            self.__renderMini(self.indexX, self.indexY)

    def __update_pos(self, layer):
        self.renX = self.mouse.x
        self.renY = self.mouse.y
        self.x = layer.toLayerPixelX(self.renX)
        self.y = layer.toLayerPixelY(self.renY)
        self.tileX = int(self.x // self.ts)
        self.tileY = int(self.y // self.ts)

    def __placeTile(self, layer):
        tile = layer.getTile(self.tileX, self.tileY)
        # do not do anything, if the requested tile does not exist or when it is already there
        if tile == self.index: return
        if self.setTilesCmd is None:
            self.setTilesCmd = SetTilesCmd(layer, self.index)

        self.setTilesCmd.addLine(self.tileX, self.tileY)

    def setIndex(self, index: int, indexY:int=None):
        if self.mouse.left: return
        main = self.main

        # set all 4 buttons to normal
        for button in (main.previewLeft, main.previewRight, main.previewUp, main.previewDown):
            button.configure(state="normal")

        # limit index to texture
        if indexY is not None:
            indexX = index
            if indexX < 0: indexX = 0
            if indexX >= self.miniTileW: indexX = self.miniTileW - 1
            if indexY < 0: indexY = 0
            if indexY >= self.miniTileH: indexY = self.miniTileH - 1
            self.indexX, self.indexY = indexX, indexY
            index = main.toTile1D(indexX, indexY)
        else:
            if index < 0: index = 0
            if index > self.miniTileW * self.miniTileH - 1: index = self.miniTileW * self.miniTileH - 1
            indexX, indexY = main.toTile2D(index)
        self.index = index

        main.previewText.configure(
            text=f"index: {index} | x, y: ({indexX}, {indexY}) * {self.ts}: ({indexX*self.ts}, {indexY*self.ts})"
        )

        # disable the correct buttons
        if indexX == 0: main.previewLeft.configure(state="disabled")
        if indexX == self.miniTileW-1: main.previewRight.configure(state="disabled")
        if indexY == 0: main.previewUp.configure(state="disabled")
        if indexY == self.miniTileH-1: main.previewDown.configure(state="disabled")

        self.__renderMini(indexX, indexY)

    def __renderMini(self, indexX: int, indexY: int):
        # prepare the preview texture
        ag.fillTexture(self.previewTex, (0, 0, 0, 0), replace=True)
        for dy in range(-1, 1 +1):
            for dx in range(-1, 1 +1):
                x = indexX + dx
                y = indexY + dy
                if x < 0 or x >= self.miniTileW or y < 0 or y >= self.miniTileH: continue
                ag.drawRect(self.previewTex, self.miniBackgroundColor, (1+dx)*self.ts, (1+dy)*self.ts, self.ts, self.ts)
                ag.drawTexture(self.previewTex, self.miniTex, (1+dx)*self.ts, (1+dy)*self.ts, (
                    x * self.ts, y * self.ts, self.ts, self.ts
                ))

        if self.previewAniShow:
            ag.drawRect(self.previewTex, (255, 255, 255, 100), self.ts, self.ts, self.ts, self.ts)
               
        # get texture data and render the preview to the window
        prev = self.main.tilePreview
        data = ag.getTextureData(self.previewTex)
        prev.origImage = Image.frombytes("RGBA", (self.previewSize, self.previewSize), data, "raw")
        resized = prev.origImage.resize((self.previewRenderSize, self.previewRenderSize), Image.NEAREST)
        prev.image = ImageTk.PhotoImage(resized)
        prev.configure(image=prev.image)

    def setIndexLeft(self):  self.setIndex(self.indexX-1, self.indexY)
    def setIndexRight(self): self.setIndex(self.indexX+1, self.indexY)
    def setIndexUp(self):    self.setIndex(self.indexX,   self.indexY-1)
    def setIndexDown(self):  self.setIndex(self.indexX,   self.indexY+1)

    def render(self):
        layer = self.layerMan.get()
        if not layer.tileExists(self.tileX, self.tileY): return

        ts = self.ts * layer.scale
        ag.drawRect(self.main.tex, (255, 255, 255, 100),
            int(layer.x + self.tileX * ts),
            int(layer.y + self.tileY * ts),
        int(ts)+1, int(ts)+1)

import tkinter as tk

class Keyboard:
    def __init__(self, main):
        self.main = main

        self.pressed = []

        main.win.bind("<Key>", self.__onKey)

    def flush(self):
        self.pressed.clear()

    def __onKey(self, event):
        self.pressed.append(event.char)

    def getEvent(self, key: str) -> bool:
        return key in self.pressed

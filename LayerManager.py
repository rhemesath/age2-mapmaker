import tkinter as tk

from Layer import *
from CollisionLayer import *

class LayerManager:
    def __init__(self, main):
        self.main = main
        self.frame = main.layersFrame
        self._buttonConfig = {"padx": 5, "pady": 5, "sticky": "we"}

        self.layers = []
        self.buttons = []
        self.showBoxes = []
        self.i = 0
        self.l = 0
    
    def add(self, layer: Layer, invoke=True):
        if layer in self.layers: return
        self.layers.append(layer)
        self.l += 1
        self.i = self.l - 1

        name = "Layer" if type(layer) is Layer else "Collision layer"
        button = tk.Button(self.frame, text=f"{self.i}: {name}", command=lambda *a: self.onSelect(layer))
        self.buttons.append(button)
        
        def toggleShow(*args): layer.show = not layer.show
        showVar = tk.BooleanVar()
        showBox = tk.Checkbutton(self.frame, text="show", variable=showVar)
        showBox.select()
        showVar.trace("w", toggleShow)
        self.showBoxes.append((showBox, showVar))

        for i, btn in enumerate(reversed(self.buttons)):
            btn.grid(row=i, column=0, **self._buttonConfig)
            self.showBoxes[-1-i][0].grid(row=i, column=1, **self._buttonConfig)
        
        if invoke: button.invoke()

    def get(self) -> Layer:
        return self.layers[self.i]

    def addScale(self, *args, **kwargs):
        self.get().addScale(*args, **kwargs)

    def onSelect(self, layer):
        self.main.cursor.setMiniTex(layer.imagesTex)
        self.i = self.layers.index(layer)
        self.buttons[self.i].configure(state="disabled", relief="sunken")
        for i, btn in enumerate(self.buttons):
            if i == self.i: continue
            btn.configure(state="normal", relief="raised")

    def select(self, index: int):
        self.buttons[index].invoke()

    def setTex(self, imagesTex):
        for layer in self.layers: layer.setTex(imagesTex)

    def getMaps(self) -> list:
        return [layer.tileMap for layer in self.layers]

    def render(self):
        for layer in self.layers:
            layer.render()

from Layer import *

class CollisionLayer(Layer):
    bgColor = (0, 0, 0, 0)

    def __init__(self, main, tileMap: list):
        Layer.__init__(self, main, tileMap)

        self.imagesTex = None
        self.imgTileW = 2
        self.imgTileH = 1
    
    def __createPreview(self):
        if self.imagesTex is not None: ag.destroyTexture(self.imagesTex)
        self.imagesTex = ag.createTexture(self.imgTileW * self.ts, self.imgTileH * self.ts)
        
        y = 0
        for x, tile in ((0, 0), (1, 1)):
            l = x * self.ts
            t = y * self.ts

            ag.drawRect(self.imagesTex, self.bgColor, l, t, self.ts, self.ts, replace=True)
            if tile == 0: continue
            ag.drawRect(self.imagesTex, (255, 0, 0, 120), l, t, self.ts, self.ts)
            c = (0, 255, 255, 255)
            r = l + self.ts - 1
            b = t + self.ts - 1
            for x1, y1, x2, y2 in ((l, t, r, t), (r, t, r, b), (r, b, l, b), (l, b, l, t)):
                ag.drawLine(self.imagesTex, c, x1, y1, x2, y2)

    def _renderTile(self, tile: int, x: int, y: int):
        ag.drawTexture(self.tex, self.imagesTex, x * self.ts, y * self.ts, (
            (tile  % self.imgTileW) * self.ts,
            (tile // self.imgTileW) * self.ts,  
        self.ts, self.ts), replace=True)

    def setTex(self, imagesTex):
        self.cursor = self.main.cursor
        ts = self.ts
        self.ts = self.main.ts
        self.__createPreview()
        self._setMap(self.tileMap)
        self._preRender()

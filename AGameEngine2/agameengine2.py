"""Version 2.0.6-preview"""

from ctypes import c_int, c_short, byref
from os import listdir
from sdl2 import *
import sdl2.sdlgfx as gfx
from sdl2.sdlimage import *
from sdl2.sdlmixer import *

SDL_Init(SDL_INIT_EVERYTHING)

def update() -> None:
    AGE.closeRequest = False

    AGE.keyDownEvents.clear()

    AGE.mouseScrollX = 0
    AGE.mouseScrollY = 0

    while SDL_PollEvent(AGE.event):
        if AGE.event.type == SDL_QUIT: AGE.closeRequest = True
        
        elif AGE.event.type == SDL_KEYDOWN:
            AGE.keyDownEvents.append(AGE.event.key.keysym.scancode)

        elif AGE.event.type == SDL_MOUSEWHEEL:
            x = min(AGE.event.wheel.x, 1)
            y = min(AGE.event.wheel.y, 1)
            if x != 0: AGE.mouseScrollX = x
            if y != 0: AGE.mouseScrollY = y

    AGE.keys = SDL_GetKeyboardState(None)

    # Update Mouse
    AGE.prevMouseButtons = AGE.mouseButtons
    AGE.mouseButtons = SDL_GetMouseState(byref(AGE.mouseWinXC), byref(AGE.mouseWinYC) )
    AGE.mouseWinX = AGE.mouseWinXC.value
    AGE.mouseWinY = AGE.mouseWinYC.value
    AGE.mouseX = AGE.mouseWinX // AGE.winScaleX
    AGE.mouseY = AGE.mouseWinY // AGE.winScaleY

    # Calculate current FPS
    time = SDL_GetTicks()
    AGE.fpsCounterBuffer[AGE.fpsCounterIndex] = time - AGE.lastTime
    AGE.fpsCounterIndex += 1
    if AGE.fpsCounterIndex == AGE.fpsCounterBufferSize: AGE.fpsCounterIndex = 0
    fpsSum = sum(AGE.fpsCounterBuffer)
    if fpsSum == 0: fpsSum = 0.1
    AGE.fpsCounter = 1000 * AGE.fpsCounterBufferSize / fpsSum
    AGE.lastTime = time

def quit() -> None:
    if AGE.windowCreated: closeWindow()

    # Destroy all textures
    for texID in list( AGE.textures.keys() ):
        destroyTexture(AGE.textures[texID][0])

    Mix_CloseAudio()    
    Mix_Quit()
    IMG_Quit()
    SDL_Quit()

# WINDOW
def createWindow(title, width, height) -> None:
    """
    title: str
    width: int
    height: int
    """
    Mix_Init(MIX_INIT_MP3)
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1042)

    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG)
    AGE.windowCreated = True

    AGE.win = SDL_CreateWindow(title.encode(),
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height, 0
    )
    AGE.ren = SDL_CreateRenderer(AGE.win, -1, 0)
    AGE.winTex = createTexture(width, height)

    SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_BLEND)

    update()
    SDL_RenderClear(AGE.ren)
    SDL_RenderPresent(AGE.ren)

def createEmbeddedWindow(windowID) -> None:
    """
    windowID: int (hopefully)
    WARNING: No input will be registered! Not even Gamepads!
    That's up to the window AGameEngine gets embedded in
    """
    Mix_Init(MIX_INIT_MP3)
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1042)

    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG)
    
    SDL_SetHint(SDL_HINT_VIDEO_WINDOW_SHARE_PIXEL_FORMAT, b"1")
    AGE.win = SDL_CreateWindowFrom(windowID)
    AGE.ren = SDL_CreateRenderer(AGE.win, -1, 0)
    AGE.windowCreated = True

    w = c_int(0)
    h = c_int(0)
    SDL_QueryTexture(None, None, None, byref(w), byref(h) )
    width  = w.value
    height = h.value

    AGE.winTex = createTexture(width, height)

    SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_BLEND)
    AGE.blendMode = SDL_BLENDMODE_BLEND

    update()
    SDL_RenderClear(AGE.ren)

def shouldWindowClose() -> bool:
    update()
    return AGE.closeRequest

def getWindowTexture() -> SDL_Texture:
    return AGE.winTex

def updateWindow() -> None:
    SDL_SetRenderTarget(AGE.ren, None)
    SDL_RenderCopy(AGE.ren, AGE.winTex, None, None)
    SDL_RenderPresent(AGE.ren)

def setWindowTitle(title) -> None:
    """
    title: str
    """
    SDL_SetWindowTitle(AGE.win, title.encode())

def setWindowScale(scaleX, scaleY) -> None:
    """
    scaleX: int
    scaleY: int
    """
    AGE.winScaleX = scaleX
    AGE.winScaleY = scaleY

    SDL_SetWindowSize(AGE.win,
        getTextureWidth(AGE.winTex)  * scaleX,
        getTextureHeight(AGE.winTex) * scaleY
    )
    SDL_SetWindowPosition(AGE.win, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED)

def setWindowFullscreen(fullscreen, adjustScale=True) -> None:
    """
    fullscreen: bool
    adjustScale: bool
    Setzt das Fenster in den Vollbildmodus.
    Wenn adjustScale True ist, wird die Bildschirmgröße durch
    das Betriebssystem angepasst, damit das Fenster im Vollbild
    vergrößert wird.
    Andernfalls wird ein 'Fake-Vollbild' verwendet, wo der Rand des
    Fensters entfernt und der Bildschirm schwarz gefüllt wird."""
    AGE.fullscreen = fullscreen

    if fullscreen:
        if not adjustScale:
            flag   = SDL_WINDOW_FULLSCREEN_DESKTOP
        else: flag = SDL_WINDOW_FULLSCREEN
    else: flag = 0
        
    SDL_SetWindowFullscreen(AGE.win, flag)

def closeWindow() -> None:
    AGE.windowCreated = False

    if AGE.fullscreen: setWindowFullscreen(False)

    SDL_DestroyRenderer(AGE.ren)
    SDL_DestroyWindow(AGE.win)

# DISPLAY #
def getNumDisplays() -> int:
    """Gibt die Anzahl der Bildschirme zurück."""
    
    return int( SDL_GetNumVideoDisplays() )

def _getDisplayInfo(displayIndex=0) -> list:
    dm = SDL_DisplayMode()

    if SDL_GetCurrentDisplayMode(displayIndex, dm):
        raise Exception("AGameEngine: Display {} does not exist".format(displayIndex))
    
    return [int(dm.w), int(dm.h), int(dm.refresh_rate)]

def getDisplayWidth(displayIndex=0) -> int:
    """
    displayIndex: int
    """
    return _getDisplayInfo(displayIndex)[0]

def getDisplayHeight(displayIndex=0) -> int:
    """
    displayIndex: int
    """
    return _getDisplayInfo(displayIndex)[1]

def getDisplayFPS(displayIndex=0) -> int:
    """
    displayIndex: int
    """
    return _getDisplayInfo(displayIndex)[2]

# TEXTURE
def createTexture(width, height, target=True) -> SDL_Texture:
    """
    width: int
    height: int
    target: bool
    """
    if target: flag = SDL_TEXTUREACCESS_TARGET
    else:      flag = SDL_TEXTUREACCESS_STATIC

    tex = SDL_CreateTexture(AGE.ren,
        SDL_PIXELFORMAT_RGBA32, flag,
        width, height
    )
    SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_BLEND)
    AGE.textures[id(tex)] = (tex, width, height)
    return tex

def getTextureData(texture, region=None):
    """
    texture: SDL_Texture
    region: int[] - (srcX, srcY, srcW, srcH). Default: whole texture
    returns: Uint32 array
    """
    t, w, h = _getTextureInfo(texture)
    if region is None: region = (0, 0, w, h)
    AGE.drawSrcRect.x = region[0]
    AGE.drawSrcRect.y = region[1]
    AGE.drawSrcRect.w = region[2]
    AGE.drawSrcRect.h = region[3]

    pixels = (Uint32 * w * h)() # array
    SDL_SetRenderTarget(AGE.ren, texture)
    SDL_RenderReadPixels(AGE.ren, AGE.drawSrcRect, SDL_PIXELFORMAT_RGBA32, byref(pixels), w * 4) # width * (r, g, b, a)
    return pixels

def _isTexture(texture):
    if type(texture) is SDL_Texture: return True
    try:
        if type(texture.contents) is SDL_Texture: return True
    except: pass
    return False

def destroyTexture(texture) -> None:
    """
    texture: SDL_Texture
    """

    try: AGE.textures.pop( id(texture) )
    except: pass
    SDL_DestroyTexture(texture)

def _getTextureInfo(texture) -> tuple:
    if not _isTexture(texture):
        raise Exception("AGameEngine: Expected a texture")

    return AGE.textures[id(texture)]

def getTextureWidth(texture) -> int:
    """
    texture: SDL_Texture
    """
    return _getTextureInfo(texture)[1]

def getTextureHeight(texture) -> int:
    """
    texture: SDL_Texture
    """
    return _getTextureInfo(texture)[2]

def loadTexture(fileName) -> SDL_Texture:
    """
    fileName: str. One of (*.png, *.bmp, *.jpg)
    """
    sf = IMG_Load( fileName.encode() )
    if sf is None: raise Exception("AGameEngine: Failed to load image '{}'".format(fileName))
    tempTex = SDL_CreateTextureFromSurface(AGE.ren, sf)
    SDL_FreeSurface(sf)

    w = c_int(0)
    h = c_int(0)
    SDL_QueryTexture(tempTex, None, None, byref(w), byref(h) )

    tex = createTexture(w.value, h.value)
    SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_NONE)
    SDL_SetRenderTarget(AGE.ren, tex)
    SDL_RenderCopy(AGE.ren, tempTex, None, None)
    SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_BLEND)

    SDL_DestroyTexture(tempTex)
    return tex

def getTexturePixelColor(texture, x, y) -> list:
    """
    texture: SDL_Texture
    x: int
    y: int
    """
    AGE.drawSrcRect.x = x
    AGE.drawSrcRect.y = y
    AGE.drawSrcRect.w = 1
    AGE.drawSrcRect.h = 1

    data = Uint32()

    SDL_SetRenderTarget(AGE.ren, texture)
    
    SDL_RenderReadPixels(AGE.ren,
        AGE.drawSrcRect, SDL_PIXELFORMAT_RGBA32,
        byref(data), getTextureWidth(texture) * 4
    )
    data = data.value
    return [
        int( data        & 255),
        int((data >> 8 ) & 255),
        int((data >> 16) & 255),
        int((data >> 24) & 255)
    ]

def saveTexture(texture, fileName) -> None:
    """
    texture: SDL_Texture
    name: file name with path and extension. Only *.bmp is supported
    """
    # convert texture to surface
    t, w, h = _getTextureInfo(texture)
    pixels = getTextureData(texture)
    sf = SDL_CreateRGBSurfaceWithFormatFrom(pixels, w, h, 32, w * 4, SDL_PIXELFORMAT_RGBA32)

    # get save function
    if fileName.endswith(".bmp"): save = SDL_SaveBMP
    else: raise ValueError("AGameEngine: File name must end with '.bmp'")

    save(sf, fileName.encode())
    SDL_FreeSurface(sf)

# DRAWING
def fillTexture(texture, color, replace=True) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    replace: bool
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    SDL_SetRenderDrawColor(AGE.ren, *color)
    if replace:
        SDL_RenderClear(AGE.ren)
    else:
        SDL_RenderFillRect(AGE.ren, None)

def drawRect(texture, color, x, y, width, height, replace=False) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x: int
    y: int
    width: int
    height: int
    replace: bool
    """
    SDL_SetRenderTarget(AGE.ren, texture)

    if replace:
        SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_NONE)

        AGE.drawDestRect.x = x
        AGE.drawDestRect.y = y
        AGE.drawDestRect.w = width
        AGE.drawDestRect.h = height

        SDL_SetRenderDrawColor(AGE.ren, *color)
        SDL_RenderFillRect(AGE.ren, AGE.drawDestRect)

        SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_BLEND)

    else:
        gfx.boxRGBA(AGE.ren, x, y, x+width-1, y+height-1, *color)

def drawPixel(texture, color, x, y, replace=True) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x: int
    y: int
    replace: bool
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    if replace:
        SDL_SetRenderDrawColor(AGE.ren, *color)
        SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_NONE)
        SDL_RenderDrawPoint(AGE.ren, x, y)
        SDL_SetRenderDrawBlendMode(AGE.ren, SDL_BLENDMODE_BLEND)
    else:
        gfx.pixelRGBA(AGE.ren, x, y, *color)

def drawTexture(destTex, srcTex, destX, destY, region=None, replace=False) -> None:
    """
    destTex: SDL_Texture
    srcTex: SDL_Texture
    destX: int
    destY: int
    region: list [x: int, y: int, width: int, height: int]
    replace: bool
    """
    if region != None:
        AGE.drawSrcRect.x = region[0]
        AGE.drawSrcRect.y = region[1]
        AGE.drawSrcRect.w = region[2]
        AGE.drawSrcRect.h = region[3]
        region = AGE.drawSrcRect
        destW = region.w
        destH = region.h
    else:
        t, destW, destH = _getTextureInfo(srcTex)

    AGE.drawDestRect.x = destX
    AGE.drawDestRect.y = destY
    AGE.drawDestRect.w = destW
    AGE.drawDestRect.h = destH

    SDL_SetRenderTarget(AGE.ren, destTex)
    if replace:
        SDL_SetTextureBlendMode(srcTex, SDL_BLENDMODE_NONE)

    SDL_RenderCopy(AGE.ren, srcTex, region, AGE.drawDestRect)

    if replace:
        SDL_SetTextureBlendMode(srcTex, SDL_BLENDMODE_BLEND)

def drawTextureMod(destTex, srcTex, destX, destY, region=None, destW=None, destH=None, angle=0.0, center=None, flipX=False, flipY=False, replace=False) -> None:
    """
    destTex: SDL_Texture
    srcTex: SDL_Texture
    destX: int
    destY: int
    destW: int
    destH: int
    region: list [x: int, y: int, width: int, height: int]
    angle: float. In Grad und im Uhrzeigersinn
    center: list [x: int, y: int]. None für Mitte von srcTex bzw. region.
    flipX: bool
    flipY: bool
    replace: bool
    """
    if region != None:
        AGE.drawSrcRect.x = region[0]
        AGE.drawSrcRect.y = region[1]
        AGE.drawSrcRect.w = region[2]
        AGE.drawSrcRect.h = region[3]
        region = AGE.drawSrcRect
        if destW == None: destW = region.w
        if destH == None: destH = region.h

    elif destW == None or destH == None:
        t, srcW, srcH = _getTextureInfo(srcTex)
        if destW == None: destW = srcW
        if destH == None: destH = srcH

    if center != None:
        AGE.drawPoint.x = center[0]
        AGE.drawPoint.y = center[1]
        center = AGE.drawPoint

    if flipX: flipX = SDL_FLIP_HORIZONTAL
    else:     flipX = SDL_FLIP_NONE
    if flipY: flipY = SDL_FLIP_VERTICAL
    else:     flipY = SDL_FLIP_NONE

    AGE.drawDestRect.x = destX
    AGE.drawDestRect.y = destY
    AGE.drawDestRect.w = destW
    AGE.drawDestRect.h = destH

    SDL_SetRenderTarget(AGE.ren, destTex)

    if replace:
        SDL_SetTextureBlendMode(srcTex, SDL_BLENDMODE_NONE)

    SDL_RenderCopyEx(AGE.ren, srcTex, region, AGE.drawDestRect, angle, center, flipX | flipY)

    if replace:
        SDL_SetTextureBlendMode(srcTex, SDL_BLENDMODE_BLEND)

def drawLine(texture, color, x1, y1, x2, y2) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x1: int
    y1: int
    x2: int
    y2: int
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    gfx.lineRGBA(AGE.ren, x1, y1, x2, y2, *color)

def drawLineAA(texture, color, x1, y1, x2, y2) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x1: int
    y1: int
    x2: int
    y2: int
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    gfx.aalineRGBA(AGE.ren, x1, y1, x2, y2, *color)

def drawCircle(texture, color, x, y, radius) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x: int
    y: int
    radius: int
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    gfx.filledCircleRGBA(AGE.ren, x, y, radius, *color)

def drawEllipse(texture, color, x, y, radiusX, radiusY) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x: int
    y: int
    radiusX: int
    radiusY: int
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    gfx.filledEllipseRGBA(AGE.ren, x, y, radiusX, radiusY, *color)

def drawTriangle(texture, color, x1, y1, x2, y2, x3, y3) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    x1: int
    y1: int
    x2: int
    y2: int
    x3: int
    y3: int
    """
    SDL_SetRenderTarget(AGE.ren, texture)
    gfx.filledTrigonRGBA(AGE.ren, x1, y1, x2, y2, x3, y3, *color)

def drawPolygon(texture, color, vertices) -> None:
    """
    texture: SDL_Texture
    color: list [r: int, g: int, b: int, a: int]
    vertices: list [ [x1: int, y1: int], [x2: int, y2: int], ..., [xN: int, yN: int] ]
    """
    numVerts = len(vertices)
    arrayX = (c_short * numVerts)(*[c_short(vertex[0]) for vertex in vertices])
    arrayY = (c_short * numVerts)(*[c_short(vertex[1]) for vertex in vertices])

    SDL_SetRenderTarget(AGE.ren, texture)
    gfx.filledPolygonRGBA(AGE.ren, arrayX, arrayY, numVerts, *color)

# TIME
def wait(seconds) -> None:
    """
    seconds: float
    """
    SDL_Delay( int(seconds * 1000) )

def setFPS(fps) -> None:
    """
    fps: int
    """
    gfx.SDL_setFramerate(AGE.fpsMan, fps)
    if fps == getDisplayFPS(): _setVSync(True)

def getFPS() -> int:
    return int( round(AGE.fpsCounter, 0) )

def waitFPS() -> None:
    if not AGE.vsync:
        gfx.SDL_framerateDelay(AGE.fpsMan)

def _setVSync(state) -> None:
    """
    state: bool
    """
    if AGE.windowCreated: return
    AGE.vsync = state
    if state: flag = b"1"
    else:     flag = b"0"
    SDL_SetHint(SDL_HINT_RENDER_VSYNC, flag)

def getTime() -> float:
    return SDL_GetTicks() / 1000

# INPUT
def getKeyStrings() -> list:
    return list(AGE.keyStrings)

def keyPressed(key) -> bool:
    """
    key: str
    """
    return AGE.keys[ AGE.keyStrings[key] ]

def wasKeyPressed(key) -> bool:
    """
    key: str
    """
    return AGE.keyStrings[key] in AGE.keyDownEvents

# MOUSE
def getMouseX() -> int:
    return AGE.mouseX

def getMouseY() -> int:
    return AGE.mouseY

def getMouseWindowX() -> int:
    return AGE.mouseWinX

def getMouseWindowY() -> int:
    return AGE.mouseWinY

def setMousePos(x, y) -> None:
    """
    x: int
    y: int
    """
    AGE.mouseX = x
    AGE.mouseY = y
    AGE.mouseWinX = x * AGE.winScaleX
    AGE.mouseWinY = y * AGE.winScaleY
    SDL_WarpMouseInWindow(AGE.win, AGE.mouseWinX, AGE.mouseWinY)

def setMouseWindowPos(x, y) -> None:
    """
    x: int
    y: int
    """
    AGE.mouseWinX = x
    AGE.mouseWinY = y
    AGE.mouseX = x // AGE.winScaleX
    AGE.mouseY = y // AGE.winScaleY
    SDL_WarpMouseInWindow(AGE.win, x, y)

def mousePressed(button) -> bool:
    """
    button: str
    """
    return AGE.mouseButtons & SDL_BUTTON(AGE.mouseButtonStrings[button])

def wasMousePressed(button) -> bool:
    """
    button: str
    """
    return mousePressed(button) and not (
        AGE.prevMouseButtons & SDL_BUTTON(AGE.mouseButtonStrings[button])
    )

def getMouseScrollX() -> int:
    """
    Scroll-Richtung ist Plattformabhängig!
    Meistens ist + nach rechts und - nach links
    """
    return AGE.mouseScrollX

def getMouseScrollY() -> int:
    """
    Scroll-Richtung ist Plattformabhängig!
    Meistens ist + nach oben und - nach unten
    """
    return AGE.mouseScrollY

def setMouseVisible(visible) -> None:
    """
    visible: bool
    """
    SDL_ShowCursor(visible)

# GAMEPAD
def getNumGamepads() -> int:
    """Gibt zurück, wie viele Gamepads mit dem Gerät
    verbunden sind."""

    return SDL_NumJoysticks()

def getGamepadName(gamepadIndex) -> str:
    """
    gamepadIndex: int
    Gibt den Namen eines Gamepads zurück.
    Das erste Gamepad hat den Index 0."""

    name = SDL_JoystickNameForIndex(gamepadIndex)
    if name != None: name = str(name, "utf-8")
    return name

def useGamepad(gamepadIndex) -> None:
    """
    gamepadIndex: int
    Bereitet das Gamepad vor, sodass Eingaben von jenem
    gelesen werden können.
    Das erste Gamepad hat den Index 0."""

    gp = SDL_JoystickOpen(gamepadIndex)
    if gp == None: return
    AGE.gpads[gamepadIndex] = gp

def getNumGamepadButtons(gamepadIndex) -> int:
    """
    gamepadIndex: int
    Gibt die Anzahl der erkannten Knöpfe eines Gamepads zurück.
    Das erste Gamepad hat den Index 0."""    

    return SDL_JoystickNumButtons(AGE.gpads[gamepadIndex])

def getNumGamepadHats(gamepadIndex) -> int:
    """
    gamepadIndex: int
    Gibt die Anzahl der erkannten Hats eines Gamepads zurück.
    Ein Hat ist ein Steuerkreuz.
    Das erste Gamepad hat den Index 0."""

    return SDL_JoystickNumHats(AGE.gpads[gamepadIndex])

def getNumGamepadAxes(gamepadIndex) -> int:
    """
    gamepadIndex: int
    Gibt die Anzahl der erkannten Achsen eines Gamepads zurück.
    Ein Analogstick hat zwei Achsen, nämlich x- und y-Achse.
    Desweiteren kann auch eine analoge Schultertaste eine Achse sein.
    Das erste Gamepad hat den Index 0."""

    return SDL_JoystickNumAxes(AGE.gpads[gamepadIndex])

def getGamepadButton(gamepadIndex, buttonIndex) -> bool:
    """
    gamepadIndex: int
    buttonIndex: int
    Gibt zurück, ob derzeitig ein Knopf eines Gamepads
    runtergedrückt ist.
    Das erste Gamepad hat den Index 0.
    Der erste Knopf hat ebenfalls den Index 0."""

    return SDL_JoystickGetButton(AGE.gpads[gamepadIndex], buttonIndex)

def getGamepadAxis(gamepadIndex, axisIndex) -> float:
    """
    gamepadIndex: int
    axisIndex: int
    Gibt den Wert einer Achse eines Gamepads zurück.
    Der Wert ist ein float von -1 bis 1.
    Ein Analogstick hat zwei Achsen, nämlich x- und y-Achse.
    Desweiteren kann auch eine analoge Schultertaste eine Achse sein.
    Das erste Gamepad hat den Index 0.
    Die erste Achse hat ebenfalls den Index 0."""

    return round(SDL_JoystickGetAxis(AGE.gpads[gamepadIndex], axisIndex) / 32767, 4)

def _getGamepadHat(gamepadIndex, hatIndex) -> tuple:
    """
    gamepadIndex: int
    hatIndex: int
    Gibt den Zustand von einem Hat eines Gamepads zurück.
    Der Zustand eines Hats (=Steuerkreuzes) wird als Tupel in der
    Form (achse1, achse2) zurückgegeben.
    Die Integer achse1 und achse2 können entweder -1, 0, oder 1 sein.
    Im Normalfall ist achse1 die x-Achse und achse2 die y-Achse, wo
    -1 nach links bzw. oben ist.
    Wird z.B. das Steuerkreuz nicht berührt, wird (0, 0) zurückgegeben.
    Wenn z.B. nach oben rechts gedrückt wird, lautet der Tupel
    dann (1, -1).
    Das erste Gamepad hat den Index 0.
    Der erste Hat hat ebenfalls den Index 0."""
    
    return AGE.gpadHatPos[SDL_JoystickGetHat(AGE.gpads[gamepadIndex], hatIndex)]

def getGamepadHatX(gamepadIndex, hatIndex) -> int:
    """
    gamepadIndex: int
    hatIndex: int
    """
    return _getGamepadHat(gamepadIndex, hatIndex)[0]

def getGamepadHatY(gamepadIndex, hatIndex) -> int:
    """
    gamepadIndex: int
    hatIndex: int
    """
    return _getGamepadHat(gamepadIndex, hatIndex)[1]

# SOUND
def loadSound(fileName) -> Mix_Chunk:
    """
    fileName: str
    Nur .wav wird unterstützt!
    """
    file = Mix_LoadWAV( fileName.encode() ).contents
    if file == None:
        raise Exception("AGameEngine: Failed to load sound '{}'".format( fileName ))

    return file

def loadSoundsFromDir(path) -> dict:
    """
    path: str. Der Pfad zu dem Ordner
    Lädt alle .wav-Audiodateien eines Ordners.
    Die Keys des Dictionarys sind die jeweiligen Dateinamen ohne Endung.
    """
    if not path.endswith("/"): path += "/"
    sounds = {}
    
    for file in listdir(path):
        if file.lower().endswith(".wav"):
            sounds[ file[:-4] ] = loadSound(path + file)

    return sounds

def playSound(sound, loops=0) -> None:
    """
    sound: Mix_Chunk
    loops: int
    """
    Mix_PlayChannel(-1, sound, loops)

def setSoundsVolume(volume) -> None:
    """
    volume: float. 0.0 >= volume >= 1.0
    Setzt die Lautstärke aller Sounds. Ändert nicht die Lautstärke der Musik.
    """
    Mix_Volume(-1, int(MIX_MAX_VOLUME * volume) )

def getSoundsVolume() -> float:
    return Mix_Volume(-1, -1) / MIX_MAX_VOLUME

def areSoundsPlaying() -> bool:
    return Mix_Playing(-1) > 0 and not Mix_Paused(-1) > 0

# MUSIC
def loadMusic(fileName) -> Mix_Music:
    """
    fileName: str
    Nur .mp3 wird unterstützt!
    """
    return Mix_LoadMUS( fileName.encode() ).contents

def playMusic(music, loops=0) -> None:
    """
    music: Mix_Music
    loops: int
    """
    Mix_PlayMusic(music, loops)

def rewindMusic() -> None:
    """
    Setzt die aktuell spielende Musik zurück zum Anfang
    """
    Mix_RewindMusic()

def setMusicPos(second) -> None:
    """
    second: float. second >= 0
    """
    rewindMusic()
    Mix_SetMusicPosition(second)

def pauseMusic() -> None:
    Mix_PauseMusic()

def unpauseMusic() -> None:
    Mix_ResumeMusic()

def stopMusic() -> None:
    Mix_HaltMusic()

def isMusicPlaying() -> bool:
    return Mix_PlayingMusic() and not Mix_PausedMusic()

def setMusicVolume(volume) -> None:
    """
    volume: float. 0.0 >= volume >= 1.0
    """
    Mix_VolumeMusic( int(MIX_MAX_VOLUME * volume) )

def getMusicVolume() -> float:
    return Mix_VolumeMusic(-1) / MIX_MAX_VOLUME

# AGAMEENGINE "STRUCT"
class AGE:
    fpsCounterBufferSize = 15

    win: SDL_Window
    ren: SDL_Renderer
    winTex: SDL_Texture
    
    winScaleX = 1
    winScaleY = 1

    fpsMan = gfx.FPSManager()
    gfx.SDL_initFramerate(fpsMan)
    fpsCounter = 0.0
    lastTime = 0
    fpsCounterBuffer = [0 for i in range(fpsCounterBufferSize)]
    fpsCounterIndex = 0

    drawDestRect = SDL_Rect()
    drawSrcRect = SDL_Rect()
    drawPoint = SDL_Point()

    textures = {}

    event = SDL_Event()
    keys = []
    keyDownEvents = []

    mouseButtons = Uint32()
    prevMouseButtons = mouseButtons
    mouseWinXC = c_int(0)
    mouseWinYC = c_int(0)
    mouseWinX = 0
    mouseWinY = 0
    mouseX = 0
    mouseY = 0
    mouseScrollX = 0
    mouseScrollY = 0

    gpads = {}

    windowCreated = False
    fullscreen = False
    closeRequest = False
    vsync = False

    keyStrings = {
        "0": SDL_SCANCODE_0,
        "1": SDL_SCANCODE_1,
        "2": SDL_SCANCODE_2,
        "3": SDL_SCANCODE_3,
        "4": SDL_SCANCODE_4,
        "5": SDL_SCANCODE_5,
        "6": SDL_SCANCODE_6,
        "7": SDL_SCANCODE_7,
        "8": SDL_SCANCODE_8,
        "9": SDL_SCANCODE_9,
        "a": SDL_SCANCODE_A,
        "b": SDL_SCANCODE_B,
        "c": SDL_SCANCODE_C,
        "d": SDL_SCANCODE_D,
        "e": SDL_SCANCODE_E,
        "f": SDL_SCANCODE_F,
        "g": SDL_SCANCODE_G,
        "h": SDL_SCANCODE_H,
        "i": SDL_SCANCODE_I,
        "j": SDL_SCANCODE_J,
        "k": SDL_SCANCODE_K,
        "l": SDL_SCANCODE_L,
        "m": SDL_SCANCODE_M,
        "n": SDL_SCANCODE_N,
        "o": SDL_SCANCODE_O,
        "p": SDL_SCANCODE_P,
        "q": SDL_SCANCODE_Q,
        "r": SDL_SCANCODE_R,
        "s": SDL_SCANCODE_S,
        "t": SDL_SCANCODE_T,
        "u": SDL_SCANCODE_U,
        "v": SDL_SCANCODE_V,
        "w": SDL_SCANCODE_W,
        "x": SDL_SCANCODE_X,
        "y": SDL_SCANCODE_Y,
        "z": SDL_SCANCODE_Z,
        "left" : SDL_SCANCODE_LEFT,
        "right": SDL_SCANCODE_RIGHT,
        "up"   : SDL_SCANCODE_UP,
        "down" : SDL_SCANCODE_DOWN,
        "alt"  : SDL_SCANCODE_LALT,
        "ctrl" : SDL_SCANCODE_LCTRL,
        "shift": SDL_SCANCODE_LSHIFT,
        "caps" : SDL_SCANCODE_CAPSLOCK,
        "tab"  : SDL_SCANCODE_TAB,
        "enter": SDL_SCANCODE_RETURN,
        "space": SDL_SCANCODE_SPACE,
        "backspace": SDL_SCANCODE_BACKSPACE,
        "escape"   : SDL_SCANCODE_ESCAPE,
        "f1" : SDL_SCANCODE_F1,
        "f2" : SDL_SCANCODE_F2,
        "f3" : SDL_SCANCODE_F3,
        "f4" : SDL_SCANCODE_F4,
        "f5" : SDL_SCANCODE_F5,
        "f6" : SDL_SCANCODE_F6,
        "f7" : SDL_SCANCODE_F7,
        "f8" : SDL_SCANCODE_F8,
        "f9" : SDL_SCANCODE_F9,
        "f10": SDL_SCANCODE_F10,
        "f11": SDL_SCANCODE_F11,
        "f12": SDL_SCANCODE_F12
    }

    mouseButtonStrings = {
        "left"  : 1,
        "middle": 2,
        "right" : 3
    }

    gpadHatPos = {
        SDL_HAT_LEFTUP   : (-1, -1),
        SDL_HAT_UP       : ( 0, -1),
        SDL_HAT_RIGHTUP  : ( 1, -1),
        SDL_HAT_LEFT     : (-1,  0),
        SDL_HAT_CENTERED : ( 0,  0),
        SDL_HAT_RIGHT    : ( 1,  0),
        SDL_HAT_LEFTDOWN : (-1,  1),
        SDL_HAT_DOWN     : ( 0,  1),
        SDL_HAT_RIGHTDOWN: ( 1,  1)
    }

if __name__ == "__main__":
    # test stuff here
    pass

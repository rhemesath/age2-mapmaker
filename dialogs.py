import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from pathlib import Path

from commands import *

def askOpenImage(initialDir="."):
    return filedialog.askopenfilename(title="Select Image", initialdir=initialDir, filetypes=(
        ("PNG files", "*.png"), ("Bitmap files", "*.bmp"), ("JPG files", "*.jpg")
    ))

class BaseDialog:
    def __init__(self, main):
        self.main = main
        self.root = main.win
        self.width  = 600
        self.height = 200
        self.cancelled = False
        self.rows = {}
        self.data = {}
        self.title = "Base Dialog"
        self.names = []
    
    def _run(self):
        self.win = tk.Toplevel(self.root)
        self.win.title(self.title)
        #self.win.geometry(f"{self.width}x{self.height}")
        self.win.columnconfigure(1, weight=1)
        self.win.bind("<Escape>", lambda e: self.cancel(e))

        for i, name in enumerate(self.names):
            self.win.rowconfigure(i, pad=8)
            label = tk.Label(self.win, text=name)
            inp = tk.Text(self.win, height=1)
            inp.bind("<Return>", lambda e: "break") # disable newlines
            inp.bind("<Tab>", lambda e: self.focusNext(e)) # tab key should focus next widget
            label.grid(row=i, column=0, sticky="w", padx=4)
            inp.grid(row=i, column=1, sticky="we")
            self.rows[name] = {"label": label, "input": inp}
        numRows = len(self.rows)

        # ok and cancel buttons
        frame = tk.Frame(self.win)
        self.win.rowconfigure(4, pad=15)
        okButton     = tk.Button(frame, text="Ok", command=self.done)
        cancelButton = tk.Button(frame, text="Cancel", command=self.cancel)
        okButton.grid(    row=0, column=0, padx=15, sticky="s")
        cancelButton.grid(row=0, column=1, padx=15, sticky="s")

        self.win.rowconfigure(numRows, weight=1)
        frame.grid(row=numRows, pady=10, column=0, columnspan=3, sticky="s")

    def done(self, event=None):
        for name, data in self.rows.items():
            self.data[name] = data["input"].get(1.0, "end").replace("\n", "")
        if not self._areInputsOK():
            self.win.lift()
            return
        self._setData()
        self.win.destroy()

    def _areInputsOK(self) -> bool: pass
    def _setData(self): pass

    def focusNext(self, event):
        event.widget.tk_focusNext().focus()
        return "break"

    def cancel(self, event=None):
        self.cancelled = True
        self.win.destroy()

    def browseFile(self, name, browseFunc, event=None):
        pathStr = browseFunc()
        self.win.lift()
        if not pathStr: return
        path = Path(pathStr).resolve()
        inp = self.rows[name]["input"]
        inp.delete(1.0, "end")
        inp.insert("end", str(path))

class NewFile(BaseDialog):
    def __init__(self, main):
        BaseDialog.__init__(self, main)

        self.title = "New map"
        # names
        self.tileSizeN = "Tile size"
        self.widthN = "Width"
        self.heightN = "Height"
        self.imageFileN = "Image file"

        self.names = [self.tileSizeN, self.widthN, self.heightN, self.imageFileN]

        # TODO potential texture cleanup

        self.run()
        
    def run(self):
        self._run()
        imageOpen = tk.Button(self.win, text="...", command=lambda: self.browseFile(self.imageFileN, askOpenImage))
        imageOpen.grid(row=len(self.rows)-1, column=2, padx=4)

    def _areInputsOK(self) -> bool:
        error = lambda text: messagebox.showerror("Error", text)

        for name in (self.tileSizeN, self.widthN, self.heightN):
            data = self.data[name]
            if data == "": error(f"{name} must be specified"); return False
            try: data = int(data)
            except: error(f"{name} must be an integer"); return False
            if data <= 0: error(f"{name} must at least be 1"); return False

        img = self.data[self.imageFileN]
        if img == "": return True
        p = Path(img)
        if not p.exists(): error(f"File '{img}' does not exist"); return False
        if p.is_dir(): error(f"Path leads to a directory"); return False

        return True

    def _setData(self):
        tileSize = int(self.data[self.tileSizeN])
        width    = int(self.data[self.widthN])
        height   = int(self.data[self.heightN])
        imgPath  =     self.data[self.imageFileN]
        if imgPath == "": imgPath = None

        self.main.setupMap(tileSize, imgPath, [
            [[0 for x in range(width)] for y in range(height)] for i in range(2)
        ])

class ChangeTex(BaseDialog):
    def __init__(self, main):
        BaseDialog.__init__(self, main)
        self.title = "Change texture"

        self.tileSizeN = "Tile size"
        self.imageFileN = "Image file"
        self.names = [self.tileSizeN, self.imageFileN]

        self.run()

    def run(self):
        self._run()
        imageOpen = tk.Button(self.win, text="...", command=lambda: self.browseFile(self.imageFileN, askOpenImage))
        imageOpen.grid(row=len(self.rows)-1, column=2, padx=4)

    def _areInputsOK(self) -> bool:
        error = lambda text: messagebox.showerror("Error", text)

        name = self.tileSizeN
        data = self.data[name]
        if data == "": error(f"{name} must be specified"); return False
        try: data = int(data)
        except: error(f"{name} must be an integer"); return False
        if data <= 0: error(f"{name} must at least be 1"); return False

        img = self.data[self.imageFileN]
        if img == "": return True
        p = Path(img)
        if not p.exists(): error(f"File '{img}' does not exist"); return False
        if p.is_dir(): error(f"Path leads to a directory"); return False

        return True

    def _setData(self):
        imgPath = self.data[self.imageFileN]
        if imgPath == "": imgPath = None
        tileSize = int(self.data[self.tileSizeN])
        self.main.cmdMan.do( SetImageFromPathCmd(self.main, tileSize, imgPath) )

import tkinter as tk
import AGameEngine2 as ag
from pathlib import Path
import json

from dialogs import *

from Mouse import *
from Keyboard import *
from commands import *

from Cursor import *
from LayerManager import *

class Main:
    def __init__(self):
        self.title = "Map Maker"
        self.width  = 1000
        self.height = 600
        self.fps = 60
        self.bgColor = (0, 0, 0, 255)

        self.halfW = self.width  // 2
        self.halfH = self.height // 2
        self.__setupTK(self.title)

        ag.createEmbeddedWindow( self.embed.winfo_id() )
        ag.setFPS(self.fps)
        self.tex = ag.getWindowTexture()

        self.mouse = Mouse(self)
        self.kb = Keyboard(self)
        self.imagesTex = None
    
        self.imgPath = None
        self.lastSavePath = None
        self.lastExportPath = None
        #self.setupMap(16, None, [[[]], [[]]])
        self.setupMap(16, "./testImages.png", [[[0 for x in range(20)] for y in range(20)] for i in range(2)])

        self.running = True
        while self.running: self.__update()

    def __update(self):
        ag.update()
        self.cursor.update()

        ag.fillTexture(self.tex, self.bgColor)
        self.layerMan.render()
        
        self.cursor.render()

        self.mouse.flush()
        self.kb.flush()
        ag.waitFPS()
        ag.updateWindow()
        self.win.update()

    def quit(self):
        if self.cmdMan.isUnsaved():
            doSave = messagebox.askyesnocancel("Save?", "Save unsaved changes before quit?")
            if doSave is None: return # user cancelled
            if doSave: self.save()
        self.running = False

    def save(self):
        if self.lastSavePath is None: self.saveAs()
        else: self.__save()

    def saveAs(self):
        path = tk.filedialog.asksaveasfilename(title="Save Map", filetypes=( ("JSON files", "*.json"), ) )
        if path in ("", None, () ): return # user cancelled
        if not path.endswith(".json"): path += ".json"
        self.lastSavePath = path
        self.__save()

    def __save(self):
        saveDict = {
            "img": self.imgPath,
            "tileSize": self.ts,
            "exportPath": self.lastExportPath,
            "layers": self.layerMan.getMaps()
        }
        jsonStr = json.dumps(saveDict, separators=(",", ": ")) # removes whitespaces from list

        try:
            with open(self.lastSavePath, "w", encoding="utf-8") as saveFile:
                saveFile.write(jsonStr)
        except Exception as e:
            tk.messagebox.showerror("Error", f"Could not save file '{self.lastSavePath}'.\nError:\n{e}")
            return
        
        self.cmdMan.save()
        self.updateTitle()

    def export(self):
        if self.lastExportPath is None: self.exportAs()
        else: self.__export()
    
    def exportAs(self):
        path = tk.filedialog.asksaveasfilename(title="Export Map", filetypes=( ("Python files", "*.py"), ) )
        if path in ("", None, () ): return # user cancelled
        if not path.endswith(".py"): path += ".py"
        self.lastExportPath = path
        self.__export()

    def __export(self):
        layers = self.layerMan.getMaps()

        def listToString(l):
            return json.dumps({"x":l}, separators=(",", ":"))[len('{"x":') : -1]

        text = "\n".join([
            f"tileSize = {self.ts}",
            f"collisionLayer = {listToString(layers[-1])}",
            f"layers = {listToString(layers[:-1])}"
        ])
        try:
            with open(self.lastExportPath, "w", encoding="utf-8") as exportFile:
                exportFile.write(text)
        except Exception as e:
            tk.messagebox.showerror("Error", f"Could not export file '{self.lastExportPath}'.\nError:\n{e}")

    def load(self):
        pathStr = tk.filedialog.askopenfilename(title="Open Map", filetypes=( ("JSON files", "*.json"), ))
        if pathStr in ("", None, ()): return
        path = Path(pathStr).resolve()
        if not path.exists(): tk.messagebox.showerror("Error", "File does not exist")
        pathStr = str(path)

        try:
            with open(pathStr, "r", encoding="utf-8") as loadFile:
                jsonStr = loadFile.read()
        except Exception as e:
            tk.messagebox.showerror("Error", f"Could not open file '{pathStr}'\nError:\n{e}")
            return
        self.lastSavePath = pathStr

        dataDict = json.loads(jsonStr)
        self.lastExportPath = dataDict.get("exportPath")
        self.setupMap(dataDict["tileSize"], dataDict["img"], dataDict["layers"])

    def setTitle(self, title: str):
        self.win.title(title)

    def updateTitle(self):
        t = self.title
        if self.lastSavePath is not None:
            t += f' - "{self.lastSavePath}"'
            if self.cmdMan.isUnsaved(): t = f"* {t} *"
        self.setTitle(t)

    def setupMap(self, tileSize: int, imagePath: str, layerMaps: list):
        if imagePath is not None: imagePath = str(Path(imagePath).resolve())
        self.cmdMan = CommandManager(self)

        self.ts = tileSize
        self.layerMan = LayerManager(self)

        for layerMap in layerMaps[:-1]:
            self.layerMan.add(Layer(self, layerMap), invoke=False)
        self.layerMan.add(CollisionLayer(self, layerMaps[-1]), invoke=False) # add collision layer but don't select it

        SetImageFromPathCmd(self, self.ts, imagePath).do()
        self.cursor.setIndex(0, 0)
        self.layerMan.select(0)
    
    def setImage(self, path, image):
        self.imgPath = path
        self.imagesTex = image
        self.imgW = ag.getTextureWidth(image)
        self.imgH = ag.getTextureHeight(image)
        self.imgTileW = self.imgW // self.ts
        self.imgTileH = self.imgH // self.ts

        self.cursor = Cursor(self)
        self.layerMan.setTex(image)
        layer = self.layerMan.get()
        layer.setScale(1.0)
        layer.centerPos()
        self.cursor.setMiniTex(layer.imagesTex)

    def __setupTK(self, title):
        self.win = tk.Tk()
        self.setTitle(title)
        self.win.protocol("WM_DELETE_WINDOW", self.quit)
        self.win.bind("<Escape>", lambda event: self.quit())

        self.embed = tk.Frame(self.win, width=self.width, height=self.height, bg="", colormap="new")
        self.embed.grid(padx=4, pady=4, sticky="nw")
        
        self.win.columnconfigure(1, weight=1)
        self.rightFrame = tk.Frame(self.win)
        self.rightFrame.columnconfigure(0, weight=1)
        self.rightFrame.grid(row=0, column=1, padx=3, pady=3, sticky="new")

        self.__setupPreview()
        self.__setupMenuBar()
        self.__setupLayers()

        self.win.bind("<Control-n>", lambda *args: NewFile(self) )
        self.win.bind("<Control-s>", lambda *args: self.save() )
        self.win.bind("<Control-o>", lambda *args: self.load() )
        self.win.bind("<Control-e>", lambda *args: self.export() )
        self.win.update()

    def __setupPreview(self):
        previewFrame = tk.Frame(self.rightFrame)
        previewFrame.grid(row=0, column=0, padx=4, pady=4, sticky="new")

        self.previewLeft  = tk.Button(previewFrame, text="←", command=lambda: self.cursor.setIndexLeft() )
        self.previewRight = tk.Button(previewFrame, text="→", command=lambda: self.cursor.setIndexRight() )
        self.previewUp    = tk.Button(previewFrame, text="↑", command=lambda: self.cursor.setIndexUp() )
        self.previewDown  = tk.Button(previewFrame, text="↓", command=lambda: self.cursor.setIndexDown() )
        self.tilePreview = tk.Label(previewFrame, relief="sunken")
        self.previewText = tk.Label(previewFrame, justify="left", font="Mono 8")

        self.previewUp.grid(row=0, column=1)
        self.previewLeft.grid(row=1, column=0)
        self.tilePreview.grid(row=1, column=1)
        self.previewRight.grid(row=1, column=2)
        self.previewDown.grid(row=2, column=1)
        self.previewText.grid(row=3, column=0, columnspan=3, sticky="w")

    def __setupLayers(self):
        self.layersFrame = tk.LabelFrame(self.rightFrame, text="Layers")
        self.layersFrame.columnconfigure(0, weight=1)
        self.layersFrame.grid(row=1, column=0, pady=10, sticky="new")

    def __setupMenuBar(self):
        self.menuBar = tk.Menu(self.win)

        self.fileMenu = tk.Menu(self.menuBar, tearoff=False)
        self.fileMenu.add_command(label="New...", command=lambda *args: NewFile(self) )
        self.fileMenu.add_command(label="Open...", command=lambda *args: self.load() )
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Save...", command=lambda *args: self.save() )
        self.fileMenu.add_command(label="Save as...", command=lambda *args: self.saveAs() )
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Export...", command=lambda *args: self.export() )
        self.fileMenu.add_command(label="Export as...", command=lambda *args: self.exportAs() )
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Exit", command=self.quit)
        self.menuBar.add_cascade(label="File", menu=self.fileMenu)

        self.editMenu = tk.Menu(self.menuBar, tearoff=False)
        self.editMenu.add_command(label="Change texture...", command=lambda *a: ChangeTex(self))
        self.menuBar.add_cascade(label="Edit", menu=self.editMenu)

        self.menuBar.add_command(label="⟲", command=lambda *args: self.cmdMan.undo())
        self.menuBar.add_command(label="⟳", command=lambda *args: self.cmdMan.redo())
        self.menuBar.add_command(label="-", command=lambda *args: self.layerMan.addScale(1-0.35, (self.halfW, self.halfH)) )
        self.menuBar.add_command(label="+", command=lambda *args: self.layerMan.addScale(1+0.35, (self.halfW, self.halfH)) )

        self.win.configure(menu=self.menuBar)
    
    def toTile2D(self, index: int) -> tuple:
        """ Returns: (x: int, y: int) """
        return (index % self.imgTileW, index // self.imgTileW)
    
    def toTile1D(self, indexX: int, indexY: int) -> int:
        return indexY * self.imgTileW + indexX

def main():
    Main()
    ag.quit()

if __name__ == "__main__":
    main()

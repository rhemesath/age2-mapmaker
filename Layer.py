import AGameEngine2 as ag

class Layer:
    bgColor = (30, 30, 30, 255)
    scale = 1.0
    minScale = 0.3
    x, y, w, h = 0.0, 0.0, 0.0, 0.0
    tileW, tileH = 0, 0
    def __init__(self, main, tileMap: list):
        self.main = main
        self.ts = main.ts
        self.show = True
        self.cursor = None
        self.tex = None

        self._setMap(tileMap)
        self.setScale(self.scale, (main.halfW, main.halfH))
        self.centerPos()
        self.maxTile = self.tileW * self.tileH - 1

    def centerPos(self):
        self.setX(self.main.halfW - self.w // 2)
        self.setY(self.main.halfH - self.h // 2)
    def setX(self, x: float): Layer.x = x
    def setY(self, y: float): Layer.y = y
    def _setMap(self, tileMap: list):
        self.tileMap = tileMap
        Layer.tileW = len(tileMap[0])
        Layer.tileH = len(tileMap)
        self.ts = self.main.ts
        Layer.w = self.tileW * self.ts
        Layer.h = self.tileH * self.ts
        if self.tex is not None: ag.destroyTexture(self.tex)
        self.tex = ag.createTexture(self.w, self.h)
        ag.fillTexture(self.tex, self.bgColor, replace=True)

    def setTex(self, imagesTex):
        self.imagesTex = imagesTex
        self.cursor = self.main.cursor
        self._setMap(self.tileMap)
        self._preRender()

    def setScale(self, scale: float, focus:tuple=None):
        """ focus: tuple - true pixel coordinates from main.embed """
        if focus is None:
            c = self.cursor
            focX, focY = c.renX, c.renY
            x, y = c.x, c.y
        else:
            focX, focY = focus
            x = self.toLayerPixelX(focX)
            y = self.toLayerPixelY(focY)

        Layer.scale = max(scale, self.minScale)
        Layer.renW = self.w * self.scale
        Layer.renH = self.h * self.scale

        self.setX(focX - x * self.scale)
        self.setY(focY - y * self.scale)

    def addScale(self, dScale: float, focus:tuple=None):
        """ focus: tuple - true pixel coordinates from main.embed """
        self.setScale(self.scale * dScale, focus)

    def toLayerPixelX(self, x: float) -> float:
        """ x - true screen pixel from main.embed
            Returns - pixel from self.tex """
        return (x - self.x) / self.scale

    def toLayerPixelY(self, y: float) -> float:
        """ y - true screen pixel from main.embed
            Returns - pixel from self.tex """
        return (y - self.y) / self.scale

    def _preRender(self):
        for y in range(self.tileH):
            line = self.tileMap[y]
            for x in range(self.tileW):
                self._renderTile(line[x], x, y)

    def _renderTile(self, tile: int, x: int, y: int):
        ag.drawRect(self.tex, self.bgColor, x * self.ts, y * self.ts, self.ts, self.ts, replace=True)
        ag.drawTexture(self.tex, self.imagesTex, x * self.ts, y * self.ts, (
            (tile  % self.main.imgTileW) * self.ts,
            (tile // self.main.imgTileW) * self.ts,  
        self.ts, self.ts))

    def tileExists(self, x: int, y: int) -> bool:
        return x >= 0 and x < self.tileW and y >= 0 and y < self.tileH

    def _getTile(self, x: int, y: int) -> int:
        return self.tileMap[y][x]
    def getTile(self, x: int, y: int) -> int:
        if self.tileExists(x, y): return self._getTile(x, y)
        return -1
    
    def _setTile(self, tile: int, x: int, y: int):
        self.tileMap[y][x] = tile
    def setTile(self, tile: int, x: int, y: int):
        if not self.tileExists(x, y): return
        self._setTile(tile, x, y)
        self._renderTile(tile, x, y)

    def render(self):
        if not self.show: return
        ag.drawTextureMod(self.main.tex, self.tex, int(self.x), int(self.y),
            destW=int(self.renW),
            destH=int(self.renH)
        )
